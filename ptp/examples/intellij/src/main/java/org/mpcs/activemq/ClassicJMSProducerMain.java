package org.mpcs.activemq;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ClassicJMSProducerMain {

    private final String connectionUri = "tcp://54.191.103.37:61616";
    private ActiveMQConnectionFactory connectionFactory;
    private QueueConnection connection;
    private QueueSession session;
    private Destination destination;

    public void before() throws Exception {
        /**
         * Create ConnectionFactory from URI
         * ActiveMQConnectionFactory implements QueueConnectionFactory
         */
        connectionFactory = new ActiveMQConnectionFactory(connectionUri);
        /**
         * Create Connection from ConnectionFactory
         */
        connection = connectionFactory.createQueueConnection();
        connection.start();

        /**
         * Create QueueSession with connection
         */
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue("ClassicTestQueue");
    }

    public void after() throws Exception {
        if (connection != null) {
            connection.close();
        }
    }

    public void run() throws Exception {
    		//Create a message producer, which knows where to send the message:  to "destination"
        MessageProducer producer = session.createProducer(destination);
        try {
            TextMessage message = session.createTextMessage();
            message.setText("Message sent from ClassicJMSProducerMain");
            producer.send(message);
        } finally {
            producer.close();
        }
    }

    public static void main(String[] args) {
        ClassicJMSProducerMain example = new ClassicJMSProducerMain();
        System.out.print("\n\n\n");
        System.out.println("Starting ClassicJMSProducerMain example now...");
        try {
            example.before();
            example.run();
            example.after();
        } catch (Exception e) {
            System.err.println("ClassicJMSProducerMain: " + e.getMessage());
        }
        System.out.println("Finished running the ClassicJMSProducerMain example.");
        System.out.print("\n\n\n");
    }
}
