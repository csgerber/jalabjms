package org.mpcs.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Enumeration;

/**
 * Created by dayj on 8/6/14.
 */
public class ClassicJMSQueueBrowser {
    private final String connectionUri = "tcp://54.191.103.37:61616";
    private ActiveMQConnectionFactory connectionFactory;
    private QueueConnection connection;
    private QueueSession session;
    private Destination destination;

    public void before() throws Exception {
        /**
         * Create ConnectionFactory from URI
         * ActiveMQConnectionFactory implements QueueConnectionFactory
         */
        connectionFactory = new ActiveMQConnectionFactory(connectionUri);

        /**
         * Create Connection from ConnectionFactory
         */
        connection = connectionFactory.createQueueConnection();
        connection.start();


        /**
         * Create QueueSession with connection
         */
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        /**
         * Use this to create a new queue
         */
        destination = session.createQueue("DayjTestQueue");
    }

    public void after() throws Exception {
        if (connection != null) {
            connection.close();
        }
    }

    public void run() throws Exception {
        //Create a message producer, which knows where to send the message:  to "destination"
        MessageProducer producer = session.createProducer(destination);
        try {
            TextMessage message1 = session.createTextMessage();
            message1.setText("Message 1");
            producer.send(message1);
            TextMessage message2 = session.createTextMessage();
            message2.setText("Message 2");
            producer.send(message2);

        } finally {
            producer.close();
        }

        QueueBrowser browser = session.createBrowser((Queue)destination);
        Enumeration messages = browser.getEnumeration();
        if ( !messages.hasMoreElements() ) {
            System.out.println("No messages in queue");
        } else {
            while (messages.hasMoreElements()) {
                TextMessage textMessage = (TextMessage)messages.nextElement();
                System.out.println("Browsing: "+ textMessage.getText());
            }
        }

        MessageConsumer consumer = session.createConsumer(destination);
        try {
            TextMessage message1 = (TextMessage) consumer.receive();
            System.out.println(message1.getText());
            TextMessage message2 = (TextMessage) consumer.receive();
            System.out.println(message2.getText());
        } finally {
            consumer.close();
        }
    }

    public static void main(String[] args) {
        ClassicJMSQueueBrowser example = new ClassicJMSQueueBrowser();
        System.out.print("\n\n\n");
        System.out.println("Starting ClassicJMSMain...");
        try {
            example.before();
            example.run();
            example.after();
        } catch (Exception e) {
            System.err.println("ClassicJMSMain: " + e.getMessage());
        }
        System.out.println("Finished running the ClassicJMSMain...");
        System.out.print("\n\n\n");
    }
}