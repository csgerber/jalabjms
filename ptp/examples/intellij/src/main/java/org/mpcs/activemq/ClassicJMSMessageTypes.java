package org.mpcs.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by dayj on 8/6/14.
 */
public class ClassicJMSMessageTypes {
    private final String connectionUri = "tcp://54.191.103.37:61616";
    private ActiveMQConnectionFactory connectionFactory;
    private QueueConnection connection;
    private QueueSession session;
    private Destination destination;

    public void before() throws Exception {
        /**
         * Create ConnectionFactory from URI
         * ActiveMQConnectionFactory implements QueueConnectionFactory
         */
        connectionFactory = new ActiveMQConnectionFactory(connectionUri);
        /**
         * Create Connection from ConnectionFactory
         */
        connection = connectionFactory.createQueueConnection();
        connection.start();

        /**
         * Create QueueSession with connection
         */
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        /**
         * Use this to create a new queue
         * You should probably rename this queue below to avoid conflicts.
         */
        destination = session.createQueue("MessageTypesTestQueue");
    }

    public void after() throws Exception {
        if (connection != null) {
            connection.close();
        }
    }

    public void run() throws Exception {
        //Create a message producer, which knows where to send the message:  to "destination"
        MessageProducer producer = session.createProducer(destination);
        try {
            sendTextMessage(producer, session);
            sendBytesMessage(producer, session);
            sendMapMessage(producer, session);
            sendStreamMessage(producer, session);
            sendObjectMessage(producer, session);

        } finally {
            producer.close();
        }

        MessageConsumer consumer = session.createConsumer(destination);
        try {
            receiveTextMessage(consumer);
            receiveBytesMessage(consumer);
            receiveMapMessage(consumer);
            receiveStreamMessage(consumer);
            receiveObjectMessage(consumer);
        } finally {
            consumer.close();
        }
    }

    private void sendTextMessage(MessageProducer producer, Session session) throws Exception{
        TextMessage message = session.createTextMessage();
        message.setText("JMS Text Message!");
        producer.send(message);
    }

    private void sendBytesMessage(MessageProducer producer, Session session) throws Exception{
        BytesMessage message = session.createBytesMessage();
        message.writeBytes("JMS Bytes Message!".getBytes());
        producer.send(message);
    }

    private void sendMapMessage(MessageProducer producer, Session session) throws Exception{
        MapMessage message = session.createMapMessage();
        message.setString("key", "value");
        message.setString("message", "JMS Map Message!");
        producer.send(message);
    }

    private void sendStreamMessage(MessageProducer producer, Session session) throws Exception{
        StreamMessage message = session.createStreamMessage();
        message.writeString("JMS Stream Message");
        message.writeString("!");
        producer.send(message);
    }

    private void sendObjectMessage(MessageProducer producer, Session session) throws Exception{
        ObjectMessage message = session.createObjectMessage(new CustomObject(10, "JMS Object Message", "!"));
        producer.send(message);
    }

    private void receiveTextMessage(MessageConsumer consumer) throws Exception{
        TextMessage message = (TextMessage) consumer.receive();
        System.out.println(message.getText());
    }

    private void receiveBytesMessage(MessageConsumer consumer) throws Exception{
        BytesMessage message = (BytesMessage) consumer.receive();
        byte[] buffer = new byte[18];
        message.readBytes(buffer);
        System.out.println(new String(buffer, "UTF-8"));
    }

    private void receiveMapMessage(MessageConsumer consumer) throws Exception{
        MapMessage message = (MapMessage) consumer.receive();
        System.out.println(message.getString("message"));
    }

    private void receiveStreamMessage(MessageConsumer consumer) throws Exception{
        StreamMessage message = (StreamMessage) consumer.receive();
        System.out.println(message.readString() + message.readString());
    }

    private void receiveObjectMessage(MessageConsumer consumer) throws Exception{
        ObjectMessage message = (ObjectMessage) consumer.receive();
        CustomObject obj = (CustomObject)message.getObject();
        System.out.println(obj.getMessage() + obj.getPunctuation());
    }

    public static void main(String[] args) {
        ClassicJMSMessageTypes example = new ClassicJMSMessageTypes();
        System.out.print("\n\n\n");
        System.out.println("Starting ClassicJMSMain...");
        try {
            example.before();
            example.run();
            example.after();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("ClassicJMSMessageTypes: " + e.getMessage());
        }
        System.out.println("Finished running the ClassicJMSMessageTypes...");
        System.out.print("\n\n\n");
    }
}