package org.mpcs.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by dayj on 8/6/14.
 */
public class ClassicJMSConsumerMain {

    private final String connectionUri = "tcp://54.191.103.37:61616";
    private ActiveMQConnectionFactory connectionFactory;
    private QueueConnection connection;
    private QueueSession session;
    private Destination destination;

    public void before() throws Exception {
        /**
         * Create ConnectionFactory from URI
         * ActiveMQConnectionFactory implements QueueConnectionFactory
         */
        connectionFactory = new ActiveMQConnectionFactory(connectionUri);
        /**
         * Create Connection from ConnectionFactory
         */
        connection = connectionFactory.createQueueConnection();
        connection.start();

        /**
         * Create QueueSession with connection
         */
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        destination = session.createQueue("ClassicTestQueue");

    }

    public void after() throws Exception {
        if (connection != null) {
            connection.close();
        }
    }

    public void run() throws Exception {

        MessageConsumer consumer = session.createConsumer(destination);
        try {
            TextMessage message = (TextMessage) consumer.receive();
            System.out.println(message.getText() + ":" + message.getJMSTimestamp());
        } finally {
            consumer.close();
        }
    }

    public static void main(String[] args) {
        ClassicJMSConsumerMain example = new ClassicJMSConsumerMain();
        System.out.print("\n\n\n");
        System.out.println("Starting ClassicJMSConsumerMain example now...");
        try {
            example.before();
            example.run();
            example.after();
        } catch (Exception e) {
            System.err.println("ClassicJMSConsumerMain: " + e.getMessage());
        }
        System.out.println("Finished running the ClassicJMSConsumerMain example.");
        System.out.print("\n\n\n");
    }
}