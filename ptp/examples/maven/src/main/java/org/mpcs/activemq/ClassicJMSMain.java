package org.mpcs.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by dayj on 8/6/14.
 */
public class ClassicJMSMain {
    private final String connectionUri = "tcp://54.191.103.37:61616";
    private ActiveMQConnectionFactory connectionFactory;
    private QueueConnection connection;
    private QueueSession session;
    private Destination destination;

    public void before() throws Exception {
        /**
         * Create ConnectionFactory from URI
         * ActiveMQConnectionFactory implements QueueConnectionFactory
         */
        connectionFactory = new ActiveMQConnectionFactory(connectionUri);
        /**
         * Create Connection from ConnectionFactory
         */
        connection = connectionFactory.createQueueConnection();
        connection.start();


        /**
         * Create QueueSession with connection
         */
        session = connection.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

        /**
         * Use this to create a new queue
         */
        destination = session.createQueue("DayjTestQueue");
    }

    public void after() throws Exception {
        if (connection != null) {
            connection.close();
        }
    }

    public void run() throws Exception {
        //Create a message producer, which knows where to send the message:  to "destination"
        MessageProducer producer = session.createProducer(destination);
        try {
            TextMessage message = session.createTextMessage();
            message.setText("Classic JMS TextMessage!");
            producer.send(message);
        } finally {
            producer.close();
        }

        MessageConsumer consumer = session.createConsumer(destination);
        try {
            TextMessage message = (TextMessage) consumer.receive();
            System.out.println(message.getText());
        } finally {
            consumer.close();
        }
    }

    public static void main(String[] args) {
        ClassicJMSMain example = new ClassicJMSMain();
        System.out.print("\n\n\n");
        System.out.println("Starting ClassicJMSMain...");
        try {
            example.before();
            example.run();
            example.after();
        } catch (Exception e) {
            System.err.println("ClassicJMSMain: " + e.getMessage());
        }
        System.out.println("Finished running the ClassicJMSMain...");
        System.out.print("\n\n\n");
    }
}