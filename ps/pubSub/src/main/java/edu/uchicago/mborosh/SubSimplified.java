/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.mborosh;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import static javax.jms.JMSContext.AUTO_ACKNOWLEDGE;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author mb
 * Used base code from the JMS spec (JMS20-5.pdf, in resources folder)
 */
public class SubSimplified {

//Can create the JMSConnectionFactory by injection
@Inject
@JMSConnectionFactory("jms/connectionFactory")
private JMSContext context;
@Resource(lookup="jms/feedTopic")
Topic feedTopic;
Topic inboundTopic;

        //Synchronous
//public String receiveMessageNew() {
public String receiveSynch() {
        JMSConsumer consumer = context.createDurableConsumer(feedTopic, "mysub");
        return consumer.receiveBody(String.class);
    }
        //Asynchronous
    //public void receiveMessagesNew2() throws NamingException {
    public void receiveAsynch() throws NamingException {
        //InitialContext namingContext = getInitialContext();
        InitialContext namingContext = new InitialContext();
        ConnectionFactory connectionFactory = (ConnectionFactory) namingContext.lookup("jms/connectionFactory");
        Topic feedTopic = (Topic) namingContext.lookup("jms/feedTopic");
        try (JMSContext context = connectionFactory.createContext(AUTO_ACKNOWLEDGE);) {
            JMSConsumer consumer = context.createDurableConsumer(inboundTopic, "mysub");
            MessageListener messageListener = new FeedListener("");
            consumer.setMessageListener(messageListener);            
                //ToDo: implement FeedListener            
        }
    }
    
    public void receiveAsynchThreads() throws NamingException {
        //InitialContext namingContext = getInitialContext();
        InitialContext namingContext = new InitialContext();
        ConnectionFactory connectionFactory = (ConnectionFactory) namingContext.lookup("jms/connectionFactory");
        Queue dataQueue = (Queue) namingContext.lookup("jms/dataQueue");
        try (JMSContext context1
                = connectionFactory.createContext(AUTO_ACKNOWLEDGE);
                JMSContext context2
                = context1.createContext(AUTO_ACKNOWLEDGE);) {
            JMSConsumer consumer1 = context1.createConsumer(dataQueue);
            FeedListener messageListener1 = new FeedListener("One");
            consumer1.setMessageListener(messageListener1);
            JMSConsumer consumer2 = context2.createConsumer(dataQueue);
            FeedListener messageListener2 = new FeedListener("Two");
            consumer2.setMessageListener(messageListener2);
// wait for messages to be received
// details omitted
        }
    }

}

//Otherwise, without injection, for synchronous you could do something like:
/*
@Resource(lookup = "jms/connectionFactory")
 ConnectionFactory connectionFactory;
 @Resource(lookup="jms/newsFeedTopic")
 Topic newsFeedTopic;
 public String receiveMessageNew() {
 try (JMSContext context = connectionFactory.createContext();){
 JMSConsumer consumer =
 context. createDurableConsumer (newsFeedTopic,"mysub");
 return consumer.receiveBody(String.class);
 }
 }
*/
