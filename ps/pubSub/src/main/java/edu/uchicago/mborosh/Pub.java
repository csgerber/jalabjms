/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.mborosh;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.*;
import javax.naming.*;

/**
 *
 * @author mb
 * 
 * Used base code from the JMS spec (JMS20-5.pdf, in resources folder)
 */

public class Pub {
    

//Can create the JMSConnectionFactory by injection
@Inject
@JMSConnectionFactory("jms/connectionFactory")
private JMSContext context;
@Resource(lookup="jms/feedTopic")
Topic feedTopic;
Topic inboundTopic;

public String send() {
        
        //Session session=connection.createSession(false,Session.AUTO_ACKNOWLEDGE);

        //JMSProducer producer = context.createProducer(newsFeedTopic, "mysub");        
        JMSProducer producer = context.createProducer();
        TextMessage myMsg = context.createTextMessage("here is a message"); 
        //myMsg.setText("here is a message");
        producer.send(feedTopic, myMsg);    
        return "0";
    }


    
}
/*
    public void sendMessageNew(String body) throws NamingException {
        //InitialContext namingContext = getInitialContext();
        InitialContext namingContext = new InitialContext();
        ConnectionFactory connectionFactory = (ConnectionFactory) namingContext.lookup("jms/connectionFactory");
        Queue dataQueue = (Queue) namingContext.lookup("jms/dataQueue");
        try (JMSContext context = connectionFactory.createContext();) {
            context.createProducer().send(dataQueue, body);
        }
    }
*/