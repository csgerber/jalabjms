/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.mborosh;

import javax.jms.*;
//import javax.jms.Connection;
//import javax.jms.ConnectionFactory;
//import javax.jms.JMSException;
//import javax.jms.MessageConsumer;
//import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author mb
 * 
 * Used base code from the JMS spec (JMS20-5.pdf, in resources folder)
 * 
 */


public class SubClassic {
    
    private Context namingContext;
    private ConnectionFactory connectionFactory;
    private Topic feedTopic;
    // ConnectionFactory connectionFactory = new ConnectionFactory();    
    // createTopicConnection(String userName, String password);
    
    
    public void read(){
        try {
            namingContext = new InitialContext();
            connectionFactory = (ConnectionFactory) namingContext.lookup("connectionFactory");
            feedTopic = (Topic) namingContext.lookup("topic/testTopic");
        } catch (NamingException e){
            System.out.println("Naming exception");
        }
     
        try (Connection connection = connectionFactory.createConnection();){
        // use connection in this try block
        // it will be closed when try block completes
        
        //Session session=connection.createSession(Session.AUTO_ACKNOWLEDGE);
        Session session=connection.createSession(false,Session.AUTO_ACKNOWLEDGE);
        MessageConsumer consumer = session.createDurableConsumer(feedTopic,"mySubscription");
        MessageListener messageListener = new FeedListener("");
        consumer.setMessageListener(messageListener);
            
        //Do I need the below line?
        //connection.start();
        
        
        //TopicSubscriber subscriber = subSession.createSubscriber(chatTopic);
        //subscriber.setMessageListener(this);
        
        
        
            // Session is not transacted and
            // uses AUTO_ACKNOWLEDGE for message acknowledgement
        //MessageConsumer consumer = session.createConsumer(stockQueue);
            //stockQueue comes from JNDI
        
        } catch (JMSException e){
            System.out.println("Connection exception");
        }



    }
    
}
