I am running this in NetBeans using Glassfish:
1. First start glassfish server (Services-->Servers-->Glassfish, right click and select Start)
2. Then, right-click the pubsub2 project and select run
3. Then, you can access the rest endpoint for the publisher:
	http://localhost:8080/pubsub2/rest/publisher/send1 (sends a message to topic 1)
	http://localhost:8080/pubsub2/rest/publisher/send2 (sends a message to topic 2)
4. There are two subscribers set up (Subscriber.java, Subscriber2.java) and they are set up to subscribe to the corresponding topic, 
and to output any messages received to the console.  You can see these messages under the Glassfish output.
