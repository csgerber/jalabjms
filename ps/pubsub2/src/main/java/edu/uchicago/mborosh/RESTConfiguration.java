/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.mborosh;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
        
/**
 *
 * @author mb
 * Adapted from http://piotrnowicki.com/2013/05/java-ee-7-jms-2-0-with-glassfish-v4/
 */

@ApplicationPath("/rest")
public class RESTConfiguration extends Application {    
}
