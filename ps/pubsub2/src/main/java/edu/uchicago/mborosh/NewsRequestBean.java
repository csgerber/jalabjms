package edu.uchicago.mborosh;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value = "news")
@SessionScoped
public class NewsRequestBean implements Serializable {

    private String subject;    

    public NewsRequestBean() {
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}