/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.mborosh;

import java.util.UUID;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Topic;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author mb
 * Adapted from http://piotrnowicki.com/2013/05/java-ee-7-jms-2-0-with-glassfish-v4/
 */

@Stateless
@Path("/publisher")
public class Publisher {
    @Resource(lookup = "jms/myFeed")
    private Topic topic;
        
    @Resource(lookup = "jms/myFeed2")
    private Topic topic2;
        
    @Inject
    private JMSContext jmsContext;
    
    
    @GET    
    @Path("/send1")
    public String produce(){

        //BusinessObject payload = new BusinessObject(UUID.randomUUID().toString());
        String myStr = new String("Msg/Topic1:"+UUID.randomUUID().toString());
        jmsContext.createProducer().send(topic, myStr);

        return "Sent a random message to topic 1";
    }
    
    @GET    
    @Path("/send2")
    public String produce2(){

        String myStr = new String("Msg/Topic2:"+UUID.randomUUID().toString());
        jmsContext.createProducer().send(topic2, myStr);

        return "Sent a random message to topic 2";
    }
}