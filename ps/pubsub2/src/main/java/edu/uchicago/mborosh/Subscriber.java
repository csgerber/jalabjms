/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.mborosh;

import java.util.List;
import java.util.UUID;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author mb
 * Adapted from http://piotrnowicki.com/2013/05/java-ee-7-jms-2-0-with-glassfish-v4/
 */
@MessageDriven(mappedName = "jms/myFeed")
@Path("/subscriber")

public class Subscriber implements MessageListener {

    
    @Override
    public void onMessage(Message message){
        try {
            //BusinessObject payload = message.getBody(BusinessObject.class);            
            String payload = ((TextMessage)message).getText();
            System.out.println("Message received" + "Sub1"+ payload);            
        } catch (JMSException e) {
            System.err.println("Error while fetching message payload: " + e.getMessage());
        }
        
    }
    

    
}
