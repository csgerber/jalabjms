/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.mborosh;
import java.io.Serializable;

/**
 *
 * @author mb
 * * credit to PiotrNowicki
 */
public class BusinessObject implements Serializable {
    
    private final String name;
    
    public BusinessObject(final String name){
        this.name=name;
    }
    
    public String getName(){
        return name;
    }
    
    @Override
    public String toString(){
        return "BusinessObject [name = " + name + "]";
    }
}
