JMS/PubSub/Michael Borosh

Example 1: Classic API

Hotel Lab (in 3 Projects):
dayjosh/jms/pubsubHotel
dayjosh/jms/pubsubHotelNapaSubMDB
dayjosh/jms/pubsubHotelSonomaSubMDB

Example 2: Simplified API

Injection/Rest:
dayjosh/jms/pubsub2

Example 3: Simplified API

Code Snippets:
dayjosh/jms/pubSub
