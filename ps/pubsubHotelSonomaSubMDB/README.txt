Michael Borosh
JMS/PubSub Example 1:  JSF Hotel Reservation
Implementation of: http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/JMS_pub_sub/JMS_pub_sub.html

This project is made of up three projects:

1. jms/pubsubHotel
2. jms/pubsubHotelNapaSubMDB
3. jms/pubsubHotelSonomaSubMDB

To run:

1. Open projects in Netbeans
2. Start Glassfish server from Netbeans (services-->servers-->glassfish-->start)
3. Run the pubsubHotel project (I do this a first time to make sure the Topics are set up properly)
4. Run the pubsubHotelNapaSubMDB project
5. Run the pubsubHotelSonomaSubMDB project
6. Run the pubsubHotel project once more
7. The http://localhost:8080/pubsubHotel/ page should load
8. Fill out the form, and check the Glassfish server output for messages

