
/*

This is an implementation of the following tutorial:
http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/JMS_pub_sub/JMS_pub_sub.html

*/
package edu.uchicago.mborosh;

import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSException;

@Named(value = "reserve")
@SessionScoped
public class ReservationRequestBean implements Serializable {

    @Inject
    private RequestProducerBean producer;
    
    private String lastName, firstName;
    private String email;
    private String dateRequested;
    private int numberOfNights;
    private int numberInParty;
    private String region;

    public ReservationRequestBean() {
    }

    public void sendRequest() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ReservationRequest request = new ReservationRequest(lastName, firstName, email, dateRequested, numberOfNights, numberInParty);
        try {
            producer.send(request, region);
            FacesMessage facesMessage = new FacesMessage("Request sent successfully");
            facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
            facesContext.addMessage(null, facesMessage);
        } catch (JMSException je) {
            FacesMessage facesMessage = new FacesMessage("Request NOT sent. Error: " + je);
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            facesContext.addMessage(null, facesMessage);
        }
    }
    
    public String getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(String dateRequested) {
        this.dateRequested = dateRequested;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getNumberInParty() {
        return numberInParty;
    }

    public void setNumberInParty(int numberInParty) {
        this.numberInParty = numberInParty;
    }

    public int getNumberOfNights() {
        return numberOfNights;
    }

    public void setNumberOfNights(int numberOfNights) {
        this.numberOfNights = numberOfNights;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
