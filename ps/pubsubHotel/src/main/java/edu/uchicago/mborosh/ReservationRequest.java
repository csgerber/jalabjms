/*

This is an implementation of the following tutorial:
http://www.oracle.com/webfolder/technetwork/tutorials/obe/java/JMS_pub_sub/JMS_pub_sub.html

*/

package edu.uchicago.mborosh;

/**
 *
 * @author mb
 */

public class ReservationRequest {
    /*
    Note: this class contains the same variables as ReservationRequestBean except
    for "region" which is our topic
    */
    private String lastName, firstName;
    private String email;
    private String dateRequested;
    private int numberOfNights;
    private int numberInParty;

    ReservationRequest(String lastName, String firstName, String email, String dateRequested, int numberOfNights, int numberInParty) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        setLastName(lastName);
        setFirstName(firstName);
        setEmail(email);
        setDateRequested(dateRequested);
        setNumberOfNights(numberOfNights);
        setNumberInParty(numberInParty);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(String dateRequested) {
        this.dateRequested = dateRequested;
    }

    public int getNumberOfNights() {
        return numberOfNights;
    }

    public void setNumberOfNights(int numberOfNights) {
        this.numberOfNights = numberOfNights;
    }

    public int getNumberInParty() {
        return numberInParty;
    }

    public void setNumberInParty(int numberInParty) {
        this.numberInParty = numberInParty;
    }
    
}
