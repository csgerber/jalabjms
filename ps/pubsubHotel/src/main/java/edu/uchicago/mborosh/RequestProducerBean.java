/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.uchicago.mborosh;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;

/**
 *
 * @author mb
 */
@Named(value = "request")
@Dependent
public class RequestProducerBean implements Serializable {
//Note this class must implement serializable as it will be passed as an object

@Resource(mappedName = "jms/myTopicH")
private Topic myTopicH;
@Resource(mappedName = "jms/myTopicFactoryH")
private ConnectionFactory myTopicFactoryH;
    /**
     * Creates a new instance of RequestProducerBean
     */
    public RequestProducerBean() {
    }
    
    public void send(ReservationRequest request, String region) throws JMSException {
        Connection connection = null;
        Session session = null;
        try {
            connection = myTopicFactoryH.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(myTopicH);
            MapMessage mm = session.createMapMessage();
            mm.setString("LastName", request.getLastName());
            mm.setString("FirstName", request.getFirstName());
            mm.setString("EMail", request.getEmail());
            mm.setString("DateRequested", request.getDateRequested());
            mm.setInt("NumberOfNights", request.getNumberOfNights());
            mm.setInt("NumberInParty", request.getNumberInParty());
            mm.setStringProperty("Region", region);
            messageProducer.send(mm);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Cannot close session", e);
                }
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
}
